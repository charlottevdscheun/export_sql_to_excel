import pyodbc
import datetime
import os, sys
import pandas as pd
import openpyxl
import subprocess
import shutil
import copy
import locale


#Function that exports the data to the designated Excel file.
def to_excel(ah,an,bh,bn,ch,cn,sh,sn):
    if db == "Table1":
        path = destination +"\\excel_file1.xlsx"
    elif db == "Table2":
        path = destination +"\\excel_file2.xlsx"

    print("writing to: " + path)
    with pd.ExcelWriter(path, engine='xlsxwriter') as writer: 
        #Model - A (huidig & nieuw)
        ah.to_excel(writer, sheet_name="Validatie - A",  startcol=0, startrow=0,index=False) 
        an.to_excel(writer, sheet_name="Validatie - A",  startcol=0, startrow=3, index=False)

        #Model - B (huidig & nieuw)
        bh.to_excel(writer, sheet_name="Validatie - B",  startcol=0, startrow=0,index=False) 
        bn.to_excel(writer, sheet_name="Validatie - B",  startcol=0, startrow=3,index=False) 
        
        #Model - C (huidig & nieuw)
        ch.to_excel(writer, sheet_name="Validatie - C",  startcol=0, startrow=0,index=False)
        cn.to_excel(writer, sheet_name="Validatie - C",  startcol=0, startrow=3,index=False) 

        #Stoplichten overzicht (huidig & nieuw)
        sh.to_excel(writer, sheet_name="Stoplichten",  startcol=0, startrow=0,index=False)
        sn.to_excel(writer, sheet_name="Stoplichten",  startcol=0, startrow=7,index=False)
    
        # Can be done better in a loop and array
        worksheetA= writer.sheets['Validatie - A'] 
        worksheetB= writer.sheets['Validatie - B'] 
        worksheetC= writer.sheets['Validatie - C'] 
        Stoplichten= writer.sheets['Stoplichten'] 

        header_list = ah.columns.values.tolist() # Generate list of headers
        for i in range(0, len(header_list)):
            worksheetA.set_column(i, i, 20) # Set column widths to 20
            worksheetB.set_column(i, i, 20)
            worksheetC.set_column(i, i, 20)
            Stoplichten.set_column(i, i,20)

        writer.save() # Save the excel file

# Function that gets the rows from SQL and puts them in a dataframe
def dataframes(cursor):
    # # Results set 1 (model A)
    column_names = [col[0] for col in cursor.description] 

    df1_data = []
    for row in cursor.fetchall():
        df1_data.append({name: row[i] for i, name in enumerate(column_names)})
    print("Model A imported")
    
    df1 = pd.DataFrame(df1_data)
    df1 = df1[df1.columns[::-1]]

    # Results set 2 (model B)
    cursor.nextset() #skip details
    cursor.nextset() #skip per department
    cursor.nextset() #skip per categorie
    cursor.nextset() #this is model B totals
    column_names = [col[0] for col in cursor.description]
    
    #make dataframe for model b
    df2_data = []
    for row in cursor.fetchall():
        df2_data.append({name: row[j] for j, name in enumerate(column_names)})
    print("Model B imported")
    df2 = pd.DataFrame(df2_data)
    df2 = df2[df2.columns[::-1]]

    # # Results set 3 (model C)
    cursor.nextset() #skip details
    cursor.nextset() #skip per department
    cursor.nextset() #skip per categorie
    cursor.nextset() #this is model C totals
    column_names = [col[0] for col in cursor.description] 

    #make dataframe for model c
    df3_data = []
    for row in cursor.fetchall():
        df3_data.append({name: row[j] for j, name in enumerate(column_names)})
    print("Model C imported")
    df3 = pd.DataFrame(df3_data)
    df3 = df3[df3.columns[::-1]]

    # # Results set 4 (stoplichten)
    cursor.nextset() #skip details
    cursor.nextset() #skip per department
    cursor.nextset() #skip per categorie
    cursor.nextset() #this is Stoplichten
    column_names = [col[0] for col in cursor.description] 

    #make dataframe for Stoplichten
    df4_data = []
    for row in cursor.fetchall():
        df4_data.append({name: row[j] for j, name in enumerate(column_names)})
    print("Stoplichten imported")
    df4 = pd.DataFrame(df4_data)
    df4 = df4[df4.columns[::-1]]
    cursor.close()
    
    return df1, df2, df3, df4

# Function to connect to the SQL server 
def cursor(db,query):
    print("Executing: " + query)
    connection = pyodbc.connect("Driver={SQL Server};"
                        "Server=server_name;"
                        "Database="+db+";"
                        )
    cursor = connection.cursor()
    cursor.execute(query)
    return cursor

# Funtion to restart the program 
def restart_program():
    python = sys.executable
    os.execl(python, python, * sys.argv)


###########################
# MAIN vanaf hier worden de definities geroepen
###########################
#get the range from the bat file arguments.
jaarweek_min = str(sys.argv[1])
jaarweek_max = str(sys.argv[2])

if not jaarweek_min < jaarweek_max:
    input("Dit is een niet geldige range. De eerste jaarweek is groter dan de laatste. Press Enter to restart..")
    restart_program()

## Get the date to put in front of the copied folder
locale.setlocale(locale.LC_TIME, "nl-BE")               #sets the language to dutch
month = str(datetime.datetime.now().strftime("%B"))     #%B gets the month name
date = str(datetime.datetime.now().strftime("%y%m"))    #"%y%m" gives back the date right now in this format: yymm (bijv. 1909)
directory = "\\Directory\Name\Where\you\want\your file\"
destination = directory + date + " - " + month


#copy the Excels from the 0. Base folder to a newly created directory 
if not os.path.exists(destination):
    os.mkdir(destination)
    print("Created directory: " + destination)
elif os.path.exists(destination):
    print("The directory already exists!")


############
## Table1
############
print("Starting with the Deka SQL scripts")
db = "Table1"
query_parameters = "EXEC db.schema.stored_procedure"
query_tabel = "EXEC db.schema.stored_procedure1"
query_huidig = "EXEC db.schema.stored_procedure2 @Jaarweek_min = "+jaarweek_min+" , @Jaarweek_max = "+jaarweek_max+""
query_nieuw =  "EXEC db.schema.stored_procedure3 @Jaarweek_min = "+jaarweek_min+" , @Jaarweek_max = "+jaarweek_max+""

#execute the parameters query
cursor_param = cursor(db,query_parameters)
cursor_param.close()

#execute the new model query
cursor_tabel = cursor(db,query_tabel)
cursor_tabel.close()

#execute the huidig model validatie
cursor_huidig = cursor(db,query_huidig)
ah,bh,ch,sh= dataframes(cursor_huidig) 
#print(sh)
#cursor_huidig.close()

#execute the nieuw model validatie
cursor_nieuw = cursor(db,query_nieuw)
an,bn,cn,sn = dataframes(cursor_nieuw)
#print(sn)
#cursor_nieuw.close()

to_excel(ah,an,bh,bn,ch,cn,sh,sn)   #all the totals

############
## Table2
############
print("Starting with Dirk")
db = "Table2"
query_parameters = "EXEC db.schema.stored_procedure"
query_tabel = "EXEC db.schema.stored_procedure1"
query_huidig = "EXEC db.schema.stored_procedure2 @Jaarweek_min = "+jaarweek_min+" , @Jaarweek_max = "+jaarweek_max+""
query_nieuw =  "EXEC db.schema.stored_procedure3 @Jaarweek_min = "+jaarweek_min+" , @Jaarweek_max = "+jaarweek_max+""

#execute the parameters query
cursor_param = cursor(db,query_parameters)
cursor_param.close()

#execute the new model query
cursor_tabel = cursor(db,query_tabel)
cursor_tabel.close()


#execute the huidig model validatie
cursor_huidig = cursor(db,query_huidig)
ah,bh,ch,sh= dataframes(cursor_huidig) 
#cursor_huidig.close()

#execute the nieuw model validatie
cursor_nieuw = cursor(db,query_nieuw)
an,bn,cn,sn = dataframes(cursor_nieuw)
#cursor_nieuw.close()

to_excel(ah,an,bh,bn,ch,cn,sh,sn)   #all the totals

input("SUCCES!! Enter to exit..")
